import React from "react";

const SpecialityTray = (props) => {
   return (  <section>
    <div className="container">
        <div className="section-header text-center">
            <h2>Choose Speciality</h2>
        </div>
        <div className="row">
            <div id="speciality-slider" className="owl-carousel">
                <div className="">
                    <div className="speciality-container">
                        <div className="speciality">
                            <img src="img/neurology.svg" className="img-fluid" />
                        </div>
                        <h3>Neurology</h3>
                    </div>
                    <div className="speciality-container">
                        <div className="speciality">
                            <img src="img/plastic-surgery.svg" className="img-fluid" />
                        </div>
                        <h3>Plastic surgery</h3>
                    </div>
                </div>
                <div className="">
                    <div className="speciality-container">
                        <div className="speciality">
                            <img src="img/neurology.svg" className="img-fluid" />
                        </div>
                        <h3>Neurology</h3>
                    </div>
                    <div className="speciality-container">
                        <div className="speciality">
                            <img src="img/plastic-surgery.svg" className="img-fluid" />
                        </div>
                        <h3>Plastic surgery</h3>
                    </div>
                </div>
                <div className="">
                    <div className="speciality-container">
                        <div className="speciality">
                            <img src="img/otology.svg" className="img-fluid" />
                        </div>
                        <h3>Otology</h3>
                    </div>
                    <div className="speciality-container">
                        <div className="speciality">
                            <img src="img/oral-health.svg" className="img-fluid" />
                        </div>
                        <h3>Oral health</h3>
                    </div>
                </div>
                <div className="">
                    <div className="speciality-container">
                        <div className="speciality">
                            <img src="img/urology.svg" className="img-fluid" />
                        </div>
                        <h3>Urology</h3>
                    </div>
                    <div className="speciality-container">
                        <div className="speciality">
                            <img src="img/orthopedics.svg" className="img-fluid" />
                        </div>
                        <h3>Orthopedics</h3>
                    </div>
                </div>
                <div className="">
                    <div className="speciality-container">
                        <div className="speciality">
                            <img src="img/urology.svg" className="img-fluid" />
                        </div>
                        <h3>Urology</h3>
                    </div>
                    <div className="speciality-container">
                        <div className="speciality">
                            <img src="img/orthopedics.svg" className="img-fluid" />
                        </div>
                        <h3>Orthopedics</h3>
                    </div>
                </div>
                <div className="">
                    <div className="speciality-container">
                        <div className="speciality">
                            <img src="img/hepatology.svg" className="img-fluid" />
                        </div>
                        <h3>Hepatology</h3>
                    </div>
                    <div className="speciality-container">
                        <div className="speciality">
                            <img src="img/pulmonology.svg" className="img-fluid" />
                        </div>
                        <h3>Pulmonology</h3>
                    </div>
                </div>
                <div className="">
                    <div className="speciality-container">
                        <div className="speciality">
                            <img src="img/neurology.svg" className="img-fluid" />
                        </div>
                        <h3>Neurology</h3>
                    </div>
                    <div className="speciality-container">
                        <div className="speciality">
                            <img src="img/plastic-surgery.svg" className="img-fluid" />
                        </div>
                        <h3>Plastic surgery</h3>
                    </div>
                </div>
                <div className="">
                    <div className="speciality-container">
                        <div className="speciality">
                            <img src="img/neurology.svg" className="img-fluid" />
                        </div>
                        <h3>Neurology</h3>
                    </div>
                    <div className="speciality-container">
                        <div className="speciality">
                            <img src="img/plastic-surgery.svg" className="img-fluid" />
                        </div>
                        <h3>Plastic surgery</h3>
                    </div>
                </div>
                <div className="">
                    <div className="speciality-container">
                        <div className="speciality">
                            <img src="img/otology.svg" className="img-fluid" />
                        </div>
                        <h3>Otology</h3>
                    </div>
                    <div className="speciality-container">
                        <div className="speciality">
                            <img src="img/oral-health.svg" className="img-fluid" />
                        </div>
                        <h3>Oral health</h3>
                    </div>
                </div>
                <div className="">
                    <div className="speciality-container">
                        <div className="speciality">
                            <img src="img/urology.svg" className="img-fluid" />
                        </div>
                        <h3>Urology</h3>
                    </div>
                    <div className="speciality-container">
                        <div className="speciality">
                            <img src="img/orthopedics.svg" className="img-fluid" />
                        </div>
                        <h3>Orthopedics</h3>
                    </div>
                </div>
                <div className="">
                    <div className="speciality-container">
                        <div className="speciality">
                            <img src="img/urology.svg" className="img-fluid" />
                        </div>
                        <h3>Urology</h3>
                    </div>
                    <div className="speciality-container">
                        <div className="speciality">
                            <img src="img/orthopedics.svg" className="img-fluid" />
                        </div>
                        <h3>Orthopedics</h3>
                    </div>
                </div>
                <div className="">
                    <div className="speciality-container">
                        <div className="speciality">
                            <img src="img/hepatology.svg" className="img-fluid" />
                        </div>
                        <h3>Hepatology</h3>
                    </div>
                    <div className="speciality-container">
                        <div className="speciality">
                            <img src="img/pulmonology.svg" className="img-fluid" />
                        </div>
                        <h3>Pulmonology</h3>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
)
}

export default SpecialityTray
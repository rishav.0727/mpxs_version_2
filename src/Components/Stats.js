import React from "react";

const Stats = (props) => {
    return (
        <section>
         <div class="container">
        <div class="row">
            <div id="stats-slider" class="owl-carousel">
                <div class="">
                    <div class="plune-stats procedure">
                        <h3>30000+</h3>
                        <h5>Procedure & Test</h5>
                    </div>
                </div>
                <div class="">
                    <div class="plune-stats doctor">
                        <h3>2000+</h3>
                        <h5>Doctors</h5>
                    </div>
                </div>
                <div class="">
                    <div class="plune-stats hospital">
                        <h3>1100+</h3>
                        <h5>Hospital</h5>
                    </div>
                </div>
                <div class="">
                    <div class="plune-stats cities">
                        <h3>25+</h3>
                        <h5>Cities</h5>
                    </div>
                </div>
            </div>
        </div>
    </div>
      </section>
    
    )
}

export default Stats
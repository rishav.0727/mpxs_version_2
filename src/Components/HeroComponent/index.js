import React from "react";

const HeroComponent = (props) => {

    return (   <section id="hero" className="d-flex align-items-center">
    <div className="container">
      <div className="row">
        <div className="col-md-12 text-center">
          <h1>World Class Healthcare <br />at <span>Affordable Prices</span></h1><br /><br />

          <h4>Book your Medical Procedure with Top Rated doctors in one click</h4><br />

        </div>

      </div>
    </div>
  </section>)
}

export default HeroComponent
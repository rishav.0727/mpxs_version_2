import React from "react";

const Procedures = (props) => {
    return (
         <React.Fragment>

<section>
    <div className="container">
        <div className="section-header text-center mg-bottom-50" >
            <h2>know your Procedure</h2>
        </div>
        <div className="row">
            <div id="procedure-slider" className="owl-carousel">
                <div className="procedure-container">
                    <img src="img/why-us.png" className="img-fluid" />
                    <h3>Hernia</h3>
                    <h5>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed</h5>
                    <a href="" className="read-more" >Read More...</a>

                    <div className="consult-btn">
                        <a href="#">
                            <button>Consult</button>
                        </a>
                    </div>
                </div>

                <div className="procedure-container">
                    <img src="img/why-us.png" className="img-fluid" />
                    <h3>Hernia</h3>
                    <h5>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed</h5>
                    <a href="" className="read-more" >Read More...</a>

                    <div className="consult-btn">
                        <a href="#">
                            <button>Consult</button>
                        </a>
                    </div>
                </div>

                <div className="procedure-container">
                    <img src="img/why-us.png" className="img-fluid" />
                    <h3>Hernia</h3>
                    <h5>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed</h5>
                    <a href="" className="read-more" >Read More...</a>

                    <div className="consult-btn">
                        <a href="#">
                            <button>Consult</button>
                        </a>
                    </div>
                </div>

                <div className="procedure-container">
                    <img src="img/why-us.png" className="img-fluid" />
                    <h3>Hernia</h3>
                    <h5>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed</h5>
                    <a href="" className="read-more" >Read More...</a>

                    <div className="consult-btn">
                        <a href="#">
                            <button>Consult</button>
                        </a>
                    </div>
                </div>

                <div className="procedure-container">
                    <img src="img/why-us.png" className="img-fluid" />
                    <h3>Hernia</h3>
                    <h5>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed</h5>
                    <a href="" className="read-more" >Read More...</a>

                    <div className="consult-btn">
                        <a href="#">
                            <button>Consult</button>
                        </a>
                    </div>
                </div>

                <div className="procedure-container">
                    <img src="img/why-us.png" className="img-fluid" />
                    <h3>Hernia</h3>
                    <h5>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed</h5>
                    <a href="" className="read-more" >Read More...</a>

                    <div className="consult-btn">
                        <a href="#">
                            <button>Consult</button>
                        </a>
                    </div>
                </div>

                <div className="procedure-container">
                    <img src="img/why-us.png" className="img-fluid" />
                    <h3>Hernia</h3>
                    <h5>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed</h5>
                    <a href="" className="read-more" >Read More...</a>

                    <div className="consult-btn">
                        <a href="#">
                            <button>Consult</button>
                        </a>
                    </div>
                </div>

                <div className="procedure-container">
                    <img src="img/why-us.png" className="img-fluid" />
                    <h3>Hernia</h3>
                    <h5>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed</h5>
                    <a href="" className="read-more" >Read More...</a>

                    <div className="consult-btn">
                        <a href="#">
                            <button>Consult</button>
                        </a>
                    </div>
                </div>

                <div className="procedure-container">
                    <img src="img/why-us.png" className="img-fluid" />
                    <h3>Hernia</h3>
                    <h5>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed</h5>
                    <a href="" className="read-more" >Read More...</a>

                    <div className="consult-btn">
                        <a href="#">
                            <button>Consult</button>
                        </a>
                    </div>
                </div>

                <div className="procedure-container">
                    <img src="img/why-us.png" className="img-fluid" />
                    <h3>Hernia</h3>
                    <h5>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed</h5>
                    <a href="" className="read-more" >Read More...</a>

                    <div className="consult-btn">
                        <a href="#">
                            <button>Consult</button>
                        </a>
                    </div>
                </div>

            </div>
        </div>
    </div>
</section>

         </React.Fragment>
    )
}


export default Procedures
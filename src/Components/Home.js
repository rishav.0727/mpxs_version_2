import React from  "react"
import DoctorsSection from "./DoctorsSection"
import Header from "./Header"
import HeroComponent from "./HeroComponent"
import Procedures from "./Procedures"
import SpecialityTray from "./SpecialityTray"
import Stats from "./Stats"
import WhyUs from "./WhyUs"

const Home = (props) =>  {

    React.useEffect(()=>{
        console.log("Inside Did Mount Of Home")
            props.runScript()
    }, [])
  
    return (
    <React.Fragment>
    <Header />
    <HeroComponent />
    <Stats />
    <SpecialityTray />
    <WhyUs />
    {/* <Procedures />
    <DoctorsSection /> */}

     </React.Fragment>)
}

export default Home
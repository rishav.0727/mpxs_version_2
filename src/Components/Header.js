import React from "react"


const Header  = (props) => {
        return (<React.Fragment>
                                          <header class="header_area">
    <div class="main_header_area animated">
        <div class="container">
            <nav id="navigation1" class="navigation">
                <div class="nav-header">
                    <a class="nav-brand" href="index.html"><img src="img/logo.png" alt=""/></a>
                    <div class="nav-toggle"></div>
                </div>
                <div class="nav-menus-wrapper">
                    <ul class="nav-menu align-to-right">
                        <li><a href="#">About us</a></li>
                        <li className="focus">
                            <a href="#">Services</a>
                            <div class="megamenu-panel" style={{right:'0', display:'block'}}>
                                <div class="megamenu-lists">
                                    <ul class="megamenu-list list-col-6">
                                        <li class="megamenu-list-title"><a href="#">Dermatology</a></li>
                                        <li><a href="#">Hair Transplant</a></li>
                                        <li><a href="#">Laser Hair Removal</a></li>
                                        <li><a href="#">Chemical Peel</a></li>
                                        <li><a href="#">Fillers</a></li>
                                        <li><a href="#">Acne Treatment</a></li>
                                    </ul>
                                    <ul class="megamenu-list list-col-6">
                                        <li class="megamenu-list-title"><a href="#">Proctologists</a></li>
                                        <li><a href="#">Piles</a></li>
                                        <li><a href="#">Fistula</a></li>
                                        <li><a href="#">Fissure</a></li>
                                        <li><a href="#">Hernia</a></li>
                                        <li><a href="#">Appendicitis</a></li>
                                        <li><a href="#">Pilonidal Sinus</a></li>
                                    </ul>
                                    <ul class="megamenu-list list-col-6">
                                        <li class="megamenu-list-title"><a href="#">Gynaecology</a></li>
                                        <li><a href="#">Hysterectomy</a></li>
                                        <li><a href="#">Cyst Removal</a></li>
                                        <li><a href="#">Normal Delivery</a></li>
                                        <li><a href="#">C-Section</a></li>
                                        <li><a href="#">MTP</a></li>
                                    </ul>
                                    <ul class="megamenu-list list-col-6">
                                        <li class="megamenu-list-title"><a href="#">ophthalmology</a></li>
                                        <li><a href="#">Cataract</a></li>
                                        <li><a href="#">Lasic</a></li>
                                        <li><a href="#">eyelid reconstruction</a></li>
                                        <li><a href="#">Glaucoma</a></li>
                                    </ul>
                                    <ul class="megamenu-list list-col-6">
                                        <li class="megamenu-list-title"><a href="#">Orthopedic</a></li>
                                        <li><a href="#">Knee Replacement</a></li>
                                        <li><a href="#">Hip Replacement</a></li>
                                        <li><a href="#">Anterior Cruciate Ligament</a></li>
                                        <li><a href="#">Shoulder Replacement</a></li>
                                    </ul>
                                    <ul class="megamenu-list list-col-6">
                                        <li class="megamenu-list-title"><a href="#">Urology</a></li>
                                        <li><a href="#">Kidney Stone</a></li>
                                        <li><a href="#">Hydrocele</a></li>
                                        <li><a href="#">Phimosis</a></li>
                                        <li><a href="#">Gall Stones</a></li>
                                        <li><a href="#">Circumcision</a></li>
                                        <li><a href="#">Cystoscopy</a></li>
                                    </ul>
                                    <ul class="megamenu-list list-col-6">
                                        <li class="megamenu-list-title"><a href="#">Urology</a></li>
                                        <li><a href="#">Kidney Stone</a></li>
                                        <li><a href="#">Hydrocele</a></li>
                                        <li><a href="#">Phimosis</a></li>
                                        <li><a href="#">Gall Stones</a></li>
                                        <li><a href="#">Circumcision</a></li>
                                        <li><a href="#">Cystoscopy</a></li>
                                    </ul>
                                    <ul class="megamenu-list list-col-6">
                                        <li class="megamenu-list-title"><a href="#">Urology</a></li>
                                        <li><a href="#">Kidney Stone</a></li>
                                        <li><a href="#">Hydrocele</a></li>
                                        <li><a href="#">Phimosis</a></li>
                                        <li><a href="#">Gall Stones</a></li>
                                        <li><a href="#">Circumcision</a></li>
                                        <li><a href="#">Cystoscopy</a></li>
                                    </ul>
                                    <ul class="megamenu-list list-col-6">
                                        <li class="megamenu-list-title"><a href="#">Urology</a></li>
                                        <li><a href="#">Kidney Stone</a></li>
                                        <li><a href="#">Hydrocele</a></li>
                                        <li><a href="#">Phimosis</a></li>
                                        <li><a href="#">Gall Stones</a></li>
                                        <li><a href="#">Circumcision</a></li>
                                        <li><a href="#">Cystoscopy</a></li>
                                    </ul>
                                </div>
                            </div>
                        </li>
                        <li><a href="#">Careers</a></li>
                        <li><a href="#">Blog</a></li>
                        <li><a href="#">Get in touch</a></li>
                    </ul>
                </div>
            </nav>
        </div>
    </div>
</header>
        </React.Fragment>)
}

export default Header
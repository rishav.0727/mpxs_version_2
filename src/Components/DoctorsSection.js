import React from "react";

const DoctorsSection = (props) => {
    return  (
        <React.Fragment>
  <section>
    <div className="container">
        <div className="section-header text-center mg-bottom-50" >
            <h2>Know the leading doctors</h2>
        </div>
        <div className="row">
            <div id="doctors-slider" className="owl-carousel">
                <div className="doctors-profile">
                    <div className="col-md-4">
                        <img src="img/why-us.png" className="img-fluid"/>
                    </div>
                    <div className="col-md-7">
                        <div className="top-hospitals">
                            <h4>Dr. Atul Mishra (New Delhi )</h4>
                            <span className="doctor-speciality">Neurologist</span>
                            <p className="speciality-hospital">Dentavision Multispeciality Dental And Eye Care Centre</p>
                            <span className="doc-experience">20 year of experience</span><br/>
                            <span className="address">Neelam Bata Rd, AC Nagar, New Industrial Town, Faridabad, Haryana 121001</span><br/>
                            <a className="direction" href="#">Get Direction</a><br/>
                            <span>Consultation Fees : <b>₹ 180</b></span>

                            <div className="doctors-btn-container">
                                <div className="col-md-6">
                                    <div className="book-btn">
                                        <button>Book</button>
                                    </div>
                                </div>
                                <div className="col-md-6">
                                    <div className="call-btn">
                                        <button><i className="fa fa-phone"></i> Call Now</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="doctors-profile">
                    <div className="col-md-4">
                        <img src="img/why-us.png" className="img-fluid"/>
                    </div>
                    <div className="col-md-7">
                        <div className="top-hospitals">
                            <h4>Dr. Atul Mishra (New Delhi )</h4>
                            <span className="doctor-speciality">Neurologist</span>
                            <p className="speciality-hospital">Dentavision Multispeciality Dental And Eye Care Centre</p>
                            <span className="doc-experience">20 year of experience</span><br/>
                            <span className="address">Neelam Bata Rd, AC Nagar, New Industrial Town, Faridabad, Haryana 121001</span><br/>
                            <a className="direction" href="#">Get Direction</a><br/>
                            <span>Consultation Fees : <b>₹ 180</b></span>

                            <div className="doctors-btn-container">
                                <div className="col-md-6">
                                    <div className="book-btn">
                                        <button>Book</button>
                                    </div>
                                </div>
                                <div className="col-md-6">
                                    <div className="call-btn">
                                        <button><i className="fa fa-phone"></i> Call Now</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="doctors-profile">
                    <div className="col-md-4">
                        <img src="img/why-us.png" className="img-fluid"/>
                    </div>
                    <div className="col-md-7">
                        <div className="top-hospitals">
                            <h4>Dr. Atul Mishra (New Delhi )</h4>
                            <span className="doctor-speciality">Neurologist</span>
                            <p className="speciality-hospital">Dentavision Multispeciality Dental And Eye Care Centre</p>
                            <span className="doc-experience">20 year of experience</span><br/>
                            <span className="address">Neelam Bata Rd, AC Nagar, New Industrial Town, Faridabad, Haryana 121001</span><br/>
                            <a className="direction" href="#">Get Direction</a><br/>
                            <span>Consultation Fees : <b>₹ 180</b></span>

                            <div className="doctors-btn-container">
                                <div className="col-md-6">
                                    <div className="book-btn">
                                        <button>Book</button>
                                    </div>
                                </div>
                                <div className="col-md-6">
                                    <div className="call-btn">
                                        <button><i className="fa fa-phone"></i> Call Now</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="doctors-profile">
                    <div className="col-md-4">
                        <img src="img/why-us.png" className="img-fluid"/>
                    </div>
                    <div className="col-md-7">
                        <div className="top-hospitals">
                            <h4>Dr. Atul Mishra (New Delhi )</h4>
                            <span className="doctor-speciality">Neurologist</span>
                            <p className="speciality-hospital">Dentavision Multispeciality Dental And Eye Care Centre</p>
                            <span className="doc-experience">20 year of experience</span><br/>
                            <span className="address">Neelam Bata Rd, AC Nagar, New Industrial Town, Faridabad, Haryana 121001</span><br/>
                            <a className="direction" href="#">Get Direction</a><br/>
                            <span>Consultation Fees : <b>₹ 180</b></span>

                            <div className="doctors-btn-container">
                                <div className="col-md-6">
                                    <div className="book-btn">
                                        <button>Book</button>
                                    </div>
                                </div>
                                <div className="col-md-6">
                                    <div className="call-btn">
                                        <button><i className="fa fa-phone"></i> Call Now</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

        </React.Fragment>
    )
}

export default DoctorsSection
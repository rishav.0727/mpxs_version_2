import React from "react";

const WhyUs  = (props) => {
        return (
            <React.Fragment>
                  <section>
    <div className="container">
        <div className="section-header text-center">
            <h2 className="mg-bottom-50">Why us</h2>
        </div>
        <div className="row text-left">
            <div className="col-md-6 order-lg-1 order-2">
                <img src="img/why-us.png" className="img-fluid" />
            </div>
            <div className="col-md-6 order-lg-2 order-1">
                <div className="why-us">
                    <h3>Discover the top rated hospitals <br/>in one click</h3>
                    <ul className="why-us-list">
                        <li><i className="fa fa-caret-right"></i> Get up to 50% off on all Your Medical Treatments.</li>
                        <li><i className="fa fa-caret-right"></i> Compare prices from the best facilities near you.</li>
                        <li><i className="fa fa-caret-right"></i> Better healthcare lesser out of pocket costs.</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>

  <section>
    <div className="container">
        <div className="row">
            <div className="col-md-6 order-lg-2 order-2 u-justify-content-flex-end">
                <img src="img/why-us.png" className="img-fluid" />
            </div>
            <div className="col-md-6 order-lg-1 order-1 text-right">
                <div className="why-us">
                    <h3>Better healthcare lesser running around</h3>
                    <ul className="why-us-list">
                        <li><i className="fa fa-caret-right"></i> Better healthcare lesser running around</li>
                        <li><i className="fa fa-caret-right"></i> Free Video Consultations</li>
                        <li><i className="fa fa-caret-right"></i> Online Insurance Claims.</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>


<section>
    <div className="container">
        <div className="row text-left">
            <div className="col-md-6 order-lg-1 order-2">
                <img src="img/why-us.png" className="img-fluid" />
            </div>
            <div className="col-md-6 order-lg-2 order-1">
                <div className="why-us">
                    <h3>India’s first platform to offer one click insurance claims.</h3>
                    <ul className="why-us-list">
                        <li><i className="fa fa-caret-right"></i> Check insurance partners of hospitals.</li>
                        <li><i className="fa fa-caret-right"></i> Enter your policy no.</li>
                        <li><i className="fa fa-caret-right"></i> Upload the documents & book.</li>
                        <li><i className="fa fa-caret-right"></i> All Individual and corporate insurances are accepted and covered.</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
            </React.Fragment>
        )
}
export default WhyUs
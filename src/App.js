import './App.css';
import './style.css'
import React,  { Suspense, lazy }  from "react";
import {  Route, Switch,  BrowserRouter as Router } from 'react-router-dom';

const Home = lazy(() => import('./Components/Home'));

function App() {
  const runScript = () => {
    if (window.$) {
      // do your action that depends on jQuery.
      let script = document.createElement('script');
      script.src = '/assets/js/bootstrap.bundle.min.js';
      script.async = true;
      script.crossorigin = 'anonymous';
      document.head.appendChild(script);

      script = document.createElement('script');
      script.src = '/assets/js/default/classy-nav.min.js';
      script.async = true;
      script.crossorigin = 'anonymous';
      document.head.appendChild(script);

      script = document.createElement('script');
      script.src = '/assets/js/default/active.js';
      script.async = true;
      script.crossorigin = 'anonymous';
      document.head.appendChild(script);

      script = document.createElement('script');
      script.src = 'https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.4.1.min.js';
      script.async = true;
      script.crossorigin = 'anonymous';
      document.head.appendChild(script);

      script = document.createElement('script');
      script.src = 'https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js';
      script.async = true;
      script.crossorigin = 'anonymous';
      document.head.appendChild(script);

    } else {
      // Load Jquey in to window

     const  script = document.createElement('script');
      script.src = '/assets/js/jquery.min.js';
      script.async = true;
      script.crossorigin = 'anonymous';
      document.head.appendChild(script);
      window.setTimeout(runScript, 50);
    }
  }

  React.useEffect(()=>{
    // runScript()
  },[])


  return (
    <React.Fragment>
      <Router>
      <Suspense fallback={<div></div>}>
        <Switch>
          <Route  
            path ="/" 
            component={()=> <Home runScript={runScript} />}
          />
        </Switch>
        </Suspense>
        </Router>
    </React.Fragment>
  );
}

export default App;
